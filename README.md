# Zena Dashboard Component

## Overview
This is a small piece of work, equivalent to a story or task that would be assigned. 

We expect this to take about 1-4 hours depending on your level of experience.

You will be given 24 hours to complete the task.

We will review the work based on the quality and determine if you are the right fit for the job.

We will be paying you a fixed amount for the work no matter the outcome of the review.

Happy Coding and Good Luck!

## The Work

Need to create a small card/widget that displays data in the `data.json` and matches the mockup `card.png`.

AdobeXD of mockup is available at https://xd.adobe.com/view/27108a5d-dc7f-4b9d-916c-2ab7e70f88b1-7243/ which provides exact details of colors, font sizes, and spacing.  But there is only desktop mockups, so use them as a guide.

### Technical Requirements/Implementation Notes
- only use vanilla js, css3, html5
- `index.html` should contain the markup
- you can have the css as either style tags or separate file
- you can have the js as either script tags or separate file
- need to pull in the data from the static json file (`data.json`) as if it is a REST api.
- optional css files for use are provided for grid and card styling. not required to use.

### Business Requirements/Acceptance Criteria
- Needs to closely match mockup in `card.png` 
- Omit any icons
- have all links/buttons go to https://www.google.com
- the card should look the same on both mobile and desktop

The card needs to look good on both desktop and mobile, you can take liberties with how to make it responsive.
